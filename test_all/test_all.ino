// Licensed under a Creative Commons Attribution 3.0 Unported License.
// Based on rcarduino.blogspot.com previous work.
// www.electrosmash.com/pedalshield

 
int in_ADC0, in_ADC1;  //variables for 2 ADCs values (ADC0, ADC1)
int POT0, POT1, POT2, out_DAC0, out_DAC1; //variables for 3 pots (ADC8, ADC9, ADC10)
int newPOT0, newPOT1, newPOT2; //variables for 3 new pot values (ADC8, ADC9, ADC10)
int LED = 3;
int FOOTSWITCH = 7; 
int TOGGLE = 2; 
 
void setup()
{
  Serial.begin(9600);
  
  //ADC Configuration
  ADC->ADC_MR |= 0x80;   // DAC in free running mode.
  ADC->ADC_CR=2;         // Starts ADC conversion.
  ADC->ADC_CHER=0x1CC0;    // Enable ADC channels 0 and 1.  
 
  //DAC Configuration
  analogWrite(DAC0,0);  // Enables DAC0
  analogWrite(DAC1,0);  // Enables DAC0
 
  //Pin Configuration
  pinMode(LED, OUTPUT);  
  pinMode(FOOTSWITCH, INPUT);     
  pinMode(TOGGLE, INPUT);     
  newPOT0 = newPOT1 = newPOT2 = 0;
}
 
void loop()
{
  //Read the ADCs
  while((ADC->ADC_ISR & 0x1CC0)!=0x1CC0);// wait for ADC 0, 1, 8, 9, 10 conversion complete.
  in_ADC0=ADC->ADC_CDR[7];           // read data from ADC0
  in_ADC1=ADC->ADC_CDR[6];           // read data from ADC1  
  POT0=ADC->ADC_CDR[10];                // read data from ADC8        
  POT1=ADC->ADC_CDR[11];                // read data from ADC9   
  POT2=ADC->ADC_CDR[12];                // read data from ADC10 

  if(POT0 > newPOT0+10 || POT0 < newPOT0-10)
  {
    Serial.print("Pot 0:");
    Serial.println(POT0);
    newPOT0 = POT0;
  }

  if(POT1 > newPOT1+10 || POT1 < newPOT1-10)
  {
    Serial.print("Pot 1:");
    Serial.println(POT1);
    newPOT1 = POT1;
  }

  if(POT2 > newPOT2+10 || POT2 < newPOT2-10)
  {
    Serial.print("Pot 2:");
    Serial.println(POT2);
    newPOT2 = POT2;
  }

 
  //Add volume feature for POT0
  out_DAC0=map(in_ADC0,0,4095,1,POT0);
  out_DAC1=map(in_ADC1,0,4095,1,POT0);
 
  //Add volume feature for POT1
  out_DAC0=map(out_DAC0,0,4095,1,POT1);
  out_DAC1=map(out_DAC1,0,4095,1,POT1);
 
  //Add volume feature for POT2
  out_DAC0=map(out_DAC0,0,4095,1,POT2);
  out_DAC1=map(out_DAC1,0,4095,1,POT2);
 
  //Write the DACs
  dacc_set_channel_selection(DACC_INTERFACE, 0);          //select DAC channel 0
  dacc_write_conversion_data(DACC_INTERFACE, out_DAC0);//write on DAC
  dacc_set_channel_selection(DACC_INTERFACE, 1);          //select DAC channel 1
  dacc_write_conversion_data(DACC_INTERFACE, out_DAC1);//write on DAC
 
  //Check the FOOTSWITCH and light the LED
    if (digitalRead(FOOTSWITCH)) digitalWrite(LED, HIGH); 
    else  digitalWrite(LED, LOW); 
 
  //Check the TOGGLE SWITCH and light the LED
    if (digitalRead(TOGGLE)) digitalWrite(LED, HIGH); 
    else  digitalWrite(LED, LOW); 
}
